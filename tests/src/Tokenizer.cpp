#include <gtest/gtest.h>
#include <Token.hpp>
#include <Token.cpp>

//This tests if the individual tokens are recognized
TEST(Tokenizer, BasicTokens)
{
    using namespace mathInterpreter;

    std::vector<Token> tokens;
    tokens = Tokenizer::tokenize("+");
    EXPECT_EQ(tokens.size(), 1);
    EXPECT_EQ(tokens[0].type, TokenType::Operator);
    EXPECT_EQ(tokens[0].optype, OperatorType::Plus);

    tokens = Tokenizer::tokenize("-");
    EXPECT_EQ(tokens.size(), 1);
    EXPECT_EQ(tokens[0].type, TokenType::Operator);
    EXPECT_EQ(tokens[0].optype, OperatorType::Minus);

    tokens = Tokenizer::tokenize("*");
    EXPECT_EQ(tokens.size(), 1);
    EXPECT_EQ(tokens[0].type, TokenType::Operator);
    EXPECT_EQ(tokens[0].optype, OperatorType::Multiply);

    tokens = Tokenizer::tokenize("/");
    EXPECT_EQ(tokens.size(), 1);
    EXPECT_EQ(tokens[0].type, TokenType::Operator);
    EXPECT_EQ(tokens[0].optype, OperatorType::Divide);

    tokens = Tokenizer::tokenize("^");
    EXPECT_EQ(tokens.size(), 1);
    EXPECT_EQ(tokens[0].type, TokenType::Operator);
    EXPECT_EQ(tokens[0].optype, OperatorType::Power);

    tokens = Tokenizer::tokenize("1");
    EXPECT_EQ(tokens.size(), 1);
    EXPECT_EQ(tokens[0].type, TokenType::Number);
    EXPECT_EQ(tokens[0].value, 1.0);

    tokens = Tokenizer::tokenize(".1");
    EXPECT_EQ(tokens.size(), 1);
    EXPECT_EQ(tokens[0].type, TokenType::Number);
    EXPECT_EQ(tokens[0].value, std::stod(".1"));

    tokens = Tokenizer::tokenize("1.1");
    EXPECT_EQ(tokens.size(), 1);
    EXPECT_EQ(tokens[0].type, TokenType::Number);
    EXPECT_EQ(tokens[0].value, std::stod("1.1"));
}

TEST(Tokenizer, ToStringOperators)
{
    using namespace mathInterpreter;
    std::vector<std::pair<Token, std::string>> tests{
        {{TokenType::Operator, OperatorType::Plus}, "+"},
        {{TokenType::Operator, OperatorType::Minus}, "-"},
        {{TokenType::Operator, OperatorType::Multiply}, "*"},
        {{TokenType::Operator, OperatorType::Divide}, "/"},
        {{TokenType::Operator, OperatorType::Power}, "^"},
    };
    for (auto test : tests)
    {
        EXPECT_EQ(test.first.toString(), test.second);
    }
}

TEST(Tokenizer, ToStringNumbers)
{
    using namespace mathInterpreter;
    std::vector<std::pair<Token, std::string>> tests{
        {{TokenType::Number, 1.0}, "1.0"},
        {{TokenType::Number, 2.5}, "2.5"},
        {{TokenType::Number, 2.5682}, "2.5682"},
        {{TokenType::Number, .35}, "0.35"},

    };
    for (auto test : tests)
    {
        auto result = test.first.toString();
        for (auto it = result.end() - 1; it >= result.begin(); it--)
        {
            if (*it == '0' && *(it - 1) != '.')
            {
                result.erase(it);
            }
            else
            {
                break;
            }
        }
        EXPECT_EQ(result, test.second);
    }
}

TEST(Tokenizer, TokenizeExpression)
{
    using namespace mathInterpreter;
    std::vector<std::pair<std::string, std::vector<Token>>> tests{
        {"1+1", {{TokenType::Number,  1.0}, {TokenType::Operator,  OperatorType::Plus}, {TokenType::Number, 1.0}}},
        {"1+1*2", {{TokenType::Number,  1.0}, {TokenType::Operator,  OperatorType::Plus}, {TokenType::Number, 1.0}, { TokenType::Operator,  OperatorType::Multiply}, {TokenType::Number, 2.0}}},
        {"1+1*2^3", {{TokenType::Number,  1.0}, {TokenType::Operator,  OperatorType::Plus}, {TokenType::Number, 1.0}, { TokenType::Operator,  OperatorType::Multiply}, {TokenType::Number, 2.0}, {TokenType::Operator,  OperatorType::Power}, {TokenType::Number, 3.0}}},
        {"1+1*2^3/4", {{TokenType::Number,  1.0}, {TokenType::Operator,  OperatorType::Plus}, {TokenType::Number, 1.0}, { TokenType::Operator,  OperatorType::Multiply}, {TokenType::Number, 2.0}, {TokenType::Operator,  OperatorType::Power}, {TokenType::Number, 3.0}, {TokenType::Operator,  OperatorType::Divide}, {TokenType::Number, 4.0}}},
        {"5-6/8^5^8-5+5", {{TokenType::Number,  5.0}, {TokenType::Operator,  OperatorType::Minus},  {TokenType::Number, 6.0}, { TokenType::Operator,  OperatorType::Divide}, {TokenType::Number, 8.0}, {TokenType::Operator,  OperatorType::Power}, {TokenType::Number, 5.0}, {TokenType::Operator,  OperatorType::Power}, {TokenType::Number, 8.0}, {TokenType::Operator,  OperatorType::Minus}, {TokenType::Number, 5.0}, {TokenType::Operator,  OperatorType::Plus}, {TokenType::Number, 5.0}}}
    };

    for (auto test : tests)
    {
        auto tokens = Tokenizer::tokenize(test.first);
        ASSERT_EQ(tokens.size(), test.second.size());
        for(size_t i = 0; i < tokens.size(); i++)
        {
            EXPECT_EQ(tokens[i].type, test.second[i].type);
            if(tokens[i].type == TokenType::Number)
            {
                EXPECT_EQ(tokens[i].value, test.second[i].value);
            }
            else if(tokens[i].type == TokenType::Operator)
            {
                EXPECT_EQ(tokens[i].optype, test.second[i].optype);
            }
        }
    }
}