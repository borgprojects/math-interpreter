#pragma once
#include <memory>
#include <Token.hpp>
#include <cmath>

namespace mathInterpreter
{
    class Function;
    class Node
    {
    public:
        virtual double evaluate() const = 0;
        virtual ~Node() = default;
        virtual std::unique_ptr<Node> copy() const = 0;
    };

    class OperatorNode : public Node
    {
        const OperatorType type;
        const std::unique_ptr<Node> operand1, operand2;

    public:
        OperatorNode(OperatorType _type, Node *n1, Node *n2) : type(_type), operand1(n1), operand2(n2) {}
        OperatorNode(OperatorType _type, std::unique_ptr<Node> &&n1, std::unique_ptr<Node> &&n2) : type(_type), operand1(std::move(n1)), operand2(std::move(n2)) {}
        virtual double evaluate() const override
        {
            switch (type)
            {
            case OperatorType::Plus:
                if (operand1 == nullptr)
                    return operand2->evaluate();
                return operand1->evaluate() + operand2->evaluate();
            case OperatorType::Minus:
                if (operand1 == nullptr)
                    return -operand2->evaluate();
                return operand1->evaluate() - operand2->evaluate();
            case OperatorType::Multiply:
                return operand1->evaluate() * operand2->evaluate();
            case OperatorType::Divide:
                return operand1->evaluate() / operand2->evaluate();
            case OperatorType::Power:
                return std::pow(operand1->evaluate(), operand2->evaluate());
            }
        }
        virtual std::unique_ptr<Node> copy() const override
        {
            return std::make_unique<OperatorNode>(type, operand1->copy(), operand2->copy());
        }
    };

    class NumberNode final : public Node
    {
        const double value;

    public:
        NumberNode(const double val) : value(val) {}
        double evaluate() const override final
        {
            return value;
        }
        std::unique_ptr<Node> copy() const override final
        {
            return std::make_unique<NumberNode>(value);
        }
    };

    class FunctionNode final : public Node
    {
        const Function &function;
        const std::vector<std::unique_ptr<Node>> arguments;
        
    public:
        FunctionNode(const Function &_function, std::vector<std::unique_ptr<Node>> &&args);
        double evaluate() const override final;
        std::unique_ptr<Node> copy() const override final;       
    };
}