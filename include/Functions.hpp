#pragma once

#include <unordered_map>
#include <string>
#include <Token.hpp>
#include <Node.hpp>

namespace mathInterpreter
{
    class Function
    {
    public:
        //TODO: allow for different return types
        virtual double evaluate(const std::vector<std::unique_ptr<Node>> &) const = 0;
        virtual ~Function(){}
    };

    class SquareRootFunction final : public Function
    {
        double evaluate(const std::vector<std::unique_ptr<Node>> &args) const override
        {
            return std::sqrt(args[0]->evaluate());
        }
    };

    class RootFunction final : public Function
    {
        double evaluate(const std::vector<std::unique_ptr<Node>> &args) const override
        {
            return std::pow(args[1]->evaluate(), 1.0 / args[0]->evaluate());
        }
    };
}