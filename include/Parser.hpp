#pragma once

#include <Node.hpp>
#include <Token.hpp>
#include <Functions.hpp>

namespace mathInterpreter
{

    //TODO: permanent solution

    std::unordered_map<std::string, std::unique_ptr<Function>> functions;

    std::unordered_map<std::string, double> constants{{"pi",M_PI}};

    class Parser
    {

        int getPriority(OperatorType type)
        {
            switch (type)
            {
            case OperatorType::Plus:
            case OperatorType::Minus:
                return 1;
            case OperatorType::Multiply:
            case OperatorType::Divide:
                return 2;
            case OperatorType::Power:
                return 3;
            }
        }

        std::shared_ptr<Node> root;

        Node *parse(const Token *const tokens, const uint length)
        {
            if (length == 0)
            {
                return nullptr;
            }

            if (length == 1)
            {
                if (tokens[0].type == TokenType::Number)
                    return new NumberNode(tokens[0].value);
                else if (tokens[0].type == TokenType::Function)
                {
                    std::vector<std::unique_ptr<Node>> args;
                    for (auto &arg : tokens[0].function.args)
                    {
                        args.emplace_back(parse(arg.data(), arg.size()));
                    }
                    if (auto func = functions.find(tokens[0].function.name); func != functions.end())
                    {
                        return new FunctionNode(*(func->second), std::move(args));
                    }
                    else
                    {
                        throw std::runtime_error("Function not found");
                    }
                }
                else if (tokens[0].type == TokenType::Constant)
                {
                    if (auto const_ = constants.find(tokens[0].constant.name); const_ != constants.end())
                    {
                        return new NumberNode(const_->second);
                    }
                    else
                    {
                        throw std::runtime_error("Constant not found");
                    }
                }
                else
                    throw std::runtime_error("Invalid expression");
            }

            if (tokens[0].type == TokenType::LParen && tokens[length - 1].type == TokenType::RParen)
            {
                return parse(tokens + 1, length - 2);
            }

            int lowest = -1;
            int lowest_priority;
            int open_parentheses = 0;

            for (int i = length - 1; i >= 0; i--)
            {
                if (tokens[i].type == TokenType::RParen)
                {
                    open_parentheses++;
                }
                else if (tokens[i].type == TokenType::LParen)
                {
                    if (open_parentheses > 0)
                        open_parentheses--;
                    else
                        throw std::runtime_error("Error closed parentheses, but never opened it");
                    //TODO: Error for other cases
                }

                if (tokens[i].type == TokenType::Operator && open_parentheses == 0)
                {
                    auto priority = getPriority(tokens[i].optype);
                    if (priority < lowest_priority || lowest == -1)
                    {
                        lowest = i;
                        lowest_priority = priority;
                    }
                }
            }
            if (lowest == -1)
            {
                throw std::runtime_error("Invalid expression");
            }
            Node *node = new OperatorNode(tokens[lowest].optype, parse(tokens, lowest), parse(tokens + lowest + 1, length - lowest - 1));
            return node;
        }

    public:
        Parser(const std::vector<Token> &tokens)
        {
            functions.emplace("root", new RootFunction());
            functions.emplace("sqrt", new SquareRootFunction());
            root = std::shared_ptr<Node>(parse(tokens.data(), tokens.size()));
        }
        std::shared_ptr<Node> &getRootNode() { return root; }
    };
}