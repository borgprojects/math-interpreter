#pragma once

#include <string>
#include <vector>
#include <string_view>

namespace mathInterpreter
{
    enum class TokenType
    {
        Number,
        Function,
        Constant,
        Operator,
        LParen,
        RParen,
    };

    enum class OperatorType
    {
        Plus = '+',
        Minus = '-',
        Multiply = '*',
        Divide = '/',
        Power = '^',
    };

    const char operators[]{'+', '-', '*', '/', '^'};

    struct Token
    {
        TokenType type;
        union
        {
            double value;
            struct
            {
                std::string name;
                std::vector<std::vector<Token>> args;
            } function;
            struct
            {
                std::string name;
            } constant;
            OperatorType optype;
        };
        int priority; //TODO:

        Token(TokenType _type, double val) : type(_type), value(val)
        {
        }
        Token(TokenType _type, const std::string_view name, const std::vector<std::vector<Token>> &args) : type(_type)
        {
            new (&function.name) std::string(name);
            new (&function.args) std::vector<std::vector<Token>>(args);
        }
        Token(TokenType _type, const std::string_view name, std::vector<std::vector<Token>> &&args) : type(_type)
        {
            new (&function.name) std::string(name);
            new (&function.args) std::vector<std::vector<Token>>(std::move(args));
        }
        Token(TokenType _type, const std::string_view name) : type(_type)
        {
            new (&constant.name) std::string(name);
        }
        Token(TokenType _type, OperatorType optype) : type(_type), optype(optype)
        {
        }
        Token(TokenType _type) : type(_type)
        {
        }

        ~Token()
        {
            if (type == TokenType::Function)
            {
                function.name.~basic_string();
                function.args.~vector();
            }
            else if (type == TokenType::Constant)
            {
                constant.name.~basic_string();
            }
        }

        Token(const Token &other)
        {
            switch (other.type)
            {
            case TokenType::Number:
                type = TokenType::Number;
                value = other.value;
                break;
            case TokenType::Function:
                type = TokenType::Function;
                new (&function.name) std::string(other.function.name);
                new (&function.args) std::vector<std::vector<Token>>(other.function.args);
                break;
            case TokenType::Operator:
                type = TokenType::Operator;
                optype = other.optype;
                break;
            case TokenType::LParen:
            case TokenType::RParen:
                type = other.type;
                break;
            case TokenType::Constant:
                type = TokenType::Constant;
                new (&constant.name) std::string(other.constant.name);
                break;
            }
        }

        Token(Token &&other)
        {
            switch (other.type)
            {
            case TokenType::Number:
                type = TokenType::Number;
                value = other.value;
                break;
            case TokenType::Function:
                type = TokenType::Function;
                new (&function.name) std::string(std::move(other.function.name));
                new (&function.args) std::vector<std::vector<Token>>(std::move(other.function.args));
                break;
            case TokenType::Operator:
                type = TokenType::Operator;
                optype = other.optype;
                break;
            case TokenType::LParen:
            case TokenType::RParen:
                type = other.type;
                break;
            case TokenType::Constant:
                type = TokenType::Constant;
                new (&constant.name) std::string(std::move(other.constant.name));
                break;
            }
        }

        Token &operator=(const Token &) = delete;
        Token &operator=(Token &&other)
        {
            if (type == TokenType::Function)
            {
                function.name.~basic_string();
                function.args.~vector();
            }
            else if (type == TokenType::Constant)
            {
                constant.name.~basic_string();
            }
            switch (other.type)
            {
            case TokenType::Number:
                type = TokenType::Number;
                value = other.value;
                break;
            case TokenType::Function:
                type = TokenType::Function;
                new (&function.name) std::string(std::move(other.function.name));
                new (&function.args) std::vector<std::vector<Token>>(std::move(other.function.args));
                break;
            case TokenType::Operator:
                type = TokenType::Operator;
                optype = other.optype;
                break;
            case TokenType::LParen:
            case TokenType::RParen:
                type = other.type;
                break;
            case TokenType::Constant:
                type = TokenType::Constant;
                new (&constant.name) std::string(std::move(other.constant.name));
                break;
            }
            return *this;
        }

        std::string toString() const
        {
            switch (type)
            {
            case TokenType::Number:
                return std::to_string(value);
            case TokenType::Operator:
                return std::string(1, static_cast<char>(optype));
            case TokenType::LParen:
                return "(";
            case TokenType::RParen:
                return ")";
            case TokenType::Function:
                return function.name;
            case TokenType::Constant:
                return constant.name;
            }
        }
    };

    class Tokenizer
    {
        Tokenizer() = delete;

    public:
        static std::vector<Token> tokenize(const std::string_view expression);
    };
}