#include <Token.hpp>
#include <stdexcept>

namespace mathInterpreter
{
    std::vector<Token> Tokenizer::tokenize(std::string_view expression)
    {
        std::vector<Token> tokens;
        for (size_t i = 0; i < expression.size(); i++)
        {
            if (expression[i] == ' ')
                continue;
            if (expression[i] == '.' || (expression[i] >= '0' && expression[i] <= '9'))
            {
                size_t length;
                auto num = std::stod(expression.substr(i).data(), &length);
                tokens.emplace_back(TokenType::Number, num);
                i += length - 1;
                continue;
            }
            for (auto &op : operators)
            {
                if (expression[i] == op)
                {
                    tokens.emplace_back(TokenType::Operator, static_cast<OperatorType>(op));
                    goto tokenize_loop_end;
                }
            }

            if (expression[i] == ':')
            {
                size_t begin_args = i;
                size_t end_args = i;
                uint open_parenthesis = 0;
                for (size_t j = i + 1; j < expression.size(); j++)
                {
                    if (expression[j] == '(')
                    {
                        if (open_parenthesis == 0)
                            begin_args = j;
                        open_parenthesis++;
                    }
                    else if (expression[j] == ')')
                    {
                        open_parenthesis--;
                        if (open_parenthesis == 0)
                        {
                            end_args = j;
                            break;
                        }
                    }
                }

                std::vector<std::vector<Token>> args;
                open_parenthesis = 0;
                size_t arg_begin = begin_args + 1;
                for (size_t j = begin_args + 1; j < end_args; j++)
                {
                    if (expression[j] == '(')
                    {
                        open_parenthesis++;
                    }
                    else if (expression[j] == ')')
                    {
                        open_parenthesis--;
                    }
                    else if (expression[j] == ',' && open_parenthesis == 0)
                    {
                        args.emplace_back(tokenize(expression.substr(arg_begin, j - arg_begin)));
                        arg_begin = j + 1;
                    }
                }
                args.emplace_back(tokenize(expression.substr(arg_begin, end_args - arg_begin)));

                tokens.emplace_back(TokenType::Function, expression.substr(i + 1, begin_args - i - 1), std::move(args));
                i = end_args;
                continue;
            }

            if (expression[i] == '$')
            {
                for (int j = i + 1; j < expression.size(); j++)
                {
                    if (expression[j] == '$')
                    {
                        tokens.emplace_back(TokenType::Constant, expression.substr(i + 1, j - i - 1));
                        i = j;
                        goto tokenize_loop_end;
                    }
                }
            }

            if (expression[i] == '(')
            {
                tokens.emplace_back(TokenType::LParen);
                continue;
            }
            if (expression[i] == ')')
            {
                tokens.emplace_back(TokenType::RParen);
                continue;
            }

            throw std::runtime_error("Unknown token ");

        tokenize_loop_end:
            continue;
        }

        //Implicit multiplication

        for (int i=1;i<tokens.size();i++)
        {
            if (tokens[i].type != TokenType::Operator && tokens[i].type != TokenType::RParen)
            {
                if (!(tokens[i-1].type == TokenType::LParen || tokens[i-1].type == TokenType::Operator))
                {
                    tokens.emplace(tokens.begin() + i, Token(TokenType::Operator, OperatorType::Multiply));
                    i++;
                }
            }
        }

        return tokens;
    }
}
