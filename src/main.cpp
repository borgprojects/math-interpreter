#include <iostream>
#include <Parser.hpp>

int main(int, char **)
{
    using namespace mathInterpreter;
    std::string input;
    std::getline(std::cin,input);
    Parser p(Tokenizer::tokenize(input));
    std::cout << p.getRootNode()->evaluate()<< std::endl;
}
