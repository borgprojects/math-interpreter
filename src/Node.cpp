#include <Node.hpp>
#include <Functions.hpp>

namespace mathInterpreter
{
    FunctionNode::FunctionNode(const Function &_function, std::vector<std::unique_ptr<Node>> &&args) : function(_function), arguments(std::move(args)) {}
    double FunctionNode::evaluate() const
    {
        //TODO: perform runtime lookup of function here or better storage
        return function.evaluate(arguments);
    }
    std::unique_ptr<Node> FunctionNode::copy() const
    {
        std::vector<std::unique_ptr<Node>> args;
        for (const auto &arg : arguments)
            args.push_back(arg->copy());
        return std::make_unique<FunctionNode>(function, std::move(args));
    }
}